# Backend Installation
* pip install virtualenv
* cd backend
* virtualenv venv
* venv\Scripts\activate
* pip install -r requirements\base.txt
* python manage.py runserver

You can now access to backend at: http://localhost:8000/

You can now access the admin section at: http://localhost:8000/admin

User: admin

Pass: q1w2e3r4

# Frontend Installation
* cd frontend
* npm install
* bower install
* grunt serve

You can now access the frontend at: http://localhost:9000/

Test with the Username: Gal Silberman