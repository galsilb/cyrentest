(function () {
	'use strict';

	angular.module('application', [])
		.config(function ($stateProvider) {

			$stateProvider
				.state('app', {
					url: '/',
					abstract: true,
					templateUrl: '../scripts/application/app.html'
				});
		});
})();
