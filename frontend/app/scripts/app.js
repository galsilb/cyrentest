(function() {
	'use strict';

	angular
		.module('frontendApp', [
			'ngAnimate',
			'ngCookies',
			'ngResource',
			'ngSanitize',
			'ngTouch',
			'ui.router',
			'permission',
			'permission.ui',

			'datatables',
			'datatables.bootstrap',
			'datatables.colreorder',
			'datatables.colvis',
			'datatables.tabletools',
			'datatables.scroller',
			'datatables.columnfilter',

			'settings',
			'application',
			'user'
		])
		.config(function ($httpProvider) {
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
			delete $httpProvider.defaults.headers.common['X-Requested-With'];
		})
		.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
			$urlRouterProvider.otherwise('/user/login');
			$urlRouterProvider.deferIntercept();
		}])
		.config(function (SETTINGS, $cookiesProvider) {
			if(SETTINGS.cookie!== undefined){
            	if(SETTINGS.cookie.domain!== undefined)
					$cookiesProvider.defaults.domain = SETTINGS.cookie.domain;
				if(SETTINGS.cookie.secure !== undefined)
					$cookiesProvider.defaults.secure = SETTINGS.cookie.secure;
			}
		})
		.run(function ($rootScope, $state, $stateParams, $urlRouter, InitializationService) {
			InitializationService.initialize().then(function () {
                $urlRouter.sync();
                $urlRouter.listen();
            });
		});
})();
