(function () {
    'use strict';

    angular.module('user')
        .controller('LoginCtrl', function ($scope, $state, $q, UserService) {

            var vm = this;
            vm.submitted = false;

            vm.submit = submit;

            activate();

            function activate() {

            }

            function submit() {
                vm.submitted = true;
                UserService.login(vm.username).then(function (current_user) {
					$state.go('user.list');
                }, function (data) {
					loginError(data);
                });
            }

            function loginError(_code){
				vm.submitted = false;
				if(_code === 2){
					vm.loginError = "User was not found!";
				}  else {
					vm.loginError = "Unknown error. Please try again.";
				}
			}
        });
})();
