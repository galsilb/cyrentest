(function () {
	'use strict';

	angular.module('user', [])
		.config(function ($stateProvider) {

			$stateProvider
				.state('user', {
					url: '/user',
					abstract: true,
					templateUrl: '../scripts/user/user.html'
				})
				.state('user.login', {
					url: '/login',
					templateUrl: '../scripts/user/login.html',
					controller: 'LoginCtrl',
					controllerAs: 'login',
					data: {
						permissions: {
							only: 'GUEST',
							redirectTo: 'user.list'
						}
					}
				})
				.state('user.logout', {
					url: '/logout',
					templateUrl: '../scripts/user/logout.html',
					controller: 'LogoutCtrl',
					controllerAs: 'logout',
					data: {
						permissions: {
							except: 'GUEST',
							redirectTo: 'user.login'
						}
					}
				}).state('user.list', {
					url: '/list',
					templateUrl: '../scripts/user/list.html',
					controller: 'ListCtrl',
					controllerAs: 'list',
					data: {
						permissions: {
							only: 'USER',
							redirectTo: 'user.login'
						}
					}
				});
		});
})();
