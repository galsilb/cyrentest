(function () {
    'use strict';

    angular.module('user')
        .controller('ListCtrl', function ($scope, $state, $q, UserService, DTColumnDefBuilder, DTOptionsBuilder) {

            var vm = this;

            vm.users = [];
            vm.changeStatusValue = null;
            vm.current_user = UserService.serviceData;

            vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withColumnFilter({
				aoColumns: [{
					type: 'text'
				}, {
					type: 'select',
					bRegex: false,
					values: ['Working', 'On Vacation', 'Business Trip']
				}]
			});
			vm.dtColumnDefs = [
				DTColumnDefBuilder.newColumnDef(0).withTitle("Username"),
				DTColumnDefBuilder.newColumnDef(1).withTitle("Current Status")
			];

			vm.getUsersList = getUsersList;
			vm.changeStatus = changeStatus;

            activate();

			function activate(){
				getUsersList();
			}

			function getUsersList(){
				return UserService.userList().then(function(response){
					vm.users = response.data;
					angular.element('.tile.refreshing').removeClass('refreshing');
				});
			}

			function changeStatus(){
				UserService.updateStatus(vm.changeStatusValue).then(function(response){
					vm.current_user.currentUser.current_status = response.data['current_status'];
					getUsersList();
				})
			}
        })
		.filter('currentStatus', function (UserService) {
            return function (input) {
            	return UserService.getStatusString(input);
            }
        });
})();
