(function () {
    'use strict';

    angular.module('user')
        .controller('LogoutCtrl', function ($scope, $state, UserService) {

            var vm = this;

            activate();

            function activate() {
				UserService.logout().then(function(){
					$state.go('user.login');
				})
            }
        });
})();
