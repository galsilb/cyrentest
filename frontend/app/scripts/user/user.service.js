(function() {
    'use strict';

    angular.module('user')
        .factory('UserService', function ($rootScope, $cookies, $http, $httpParamSerializer, SETTINGS, $state, $q, PermRoleStore) {

            var serviceData = {
                currentUser: {},
				access_token: null
            };

			activate();

            function activate(){
				getAccessToken();
			}

            return {
                serviceData: serviceData,
				getAccessToken: getAccessToken,
				setAccessToken: setAccessToken,
				clearAccessToken: clearAccessToken,
                authenticate: authenticate,
                login: login,
                logout: logout,
				userList: userList,
				updateStatus: updateStatus,
				getStatusString: getStatusString
            };

            function getAccessToken() {
				serviceData.access_token = $cookies.get('access_token');
				if(serviceData.access_token !== null){
					$http.defaults.headers.common['Authorization'] = 'Bearer ' + serviceData.access_token;
				} else {
					$http.defaults.headers.common['Authorization'] = null;
				}
                return serviceData.access_token;
            }

            function setAccessToken(_access_token) {
                $cookies.put('access_token', _access_token);
				serviceData.access_token = _access_token;
				$http.defaults.headers.common['Authorization'] = 'Bearer ' + serviceData.access_token;
            }

            function clearAccessToken(){
                $cookies.remove('access_token');
				serviceData.access_token = null;
                serviceData.currentUser = null;
				$http.defaults.headers.common['Authorization'] = null;
            }

            function authenticate() {
                var deferred = $q.defer();
                if (serviceData.access_token !== null) {
                    $http({
                        'method': 'POST',
                        'url': SETTINGS.backend + '/user/authenticate/',
						'data': {
                        	access_token: serviceData.access_token
						}
                    }).then(function (response) {
                    	serviceData.currentUser = response.data;
						if(serviceData.currentUser.pk === null) {
							clearAccessToken();
						}
						clearPermissions();
						setPermission(serviceData.currentUser.pk?'USER':'GUEST');
						deferred.resolve();
					}).catch(function (response) {
						if (response.status === 0) console.error('Could not reach API');
						clearAccessToken();
						setPermission('GUEST');
						deferred.reject();
					});
                } else {
					clearAccessToken();
					setPermission('GUEST');
					deferred.resolve();
                }
                return deferred.promise;
            }

            function login(username, password) {

                var deferred = $q.defer();
                var data = {
                    client_id: SETTINGS.clientId,
                    client_secret: SETTINGS.clientSecret,
                    grant_type: 'password',
                    username: username,
                    password: password
                };

                $http({
					url: SETTINGS.backend + "/user/login/",
					method: "POST",
					data: $httpParamSerializer(data),
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Authorization': null
					}
                }).then(function (response) {
					setAccessToken(response.data['access_token']);
                    authenticate().then(function () {
                        deferred.resolve(serviceData.currentUser);
                    }, function () {
                        deferred.reject();
                    });
                }).catch(function (response) {
                    if (response.status === 0) {
                        deferred.reject("Could not reach API");
                    } else if(response.data['error_code']) {
                        deferred.reject(response.data['error_code']);
                    } else {
						deferred.reject(0);
					}
                });

                return deferred.promise;
            }

            function logout() {
            	var deferred = $q.defer();
                if (serviceData.access_token === null) {
					clearAccessToken();
					setPermission('GUEST', []);
					deferred.resolve();
				} else {
					$http({
						url: SETTINGS.backend + "/user/logout/",
						method: "POST",
						data: {
							access_token: serviceData.access_token
						}
					}).then(function () {
						clearAccessToken();
						authenticate().then(function () {
							deferred.resolve();
						}, function () {
							deferred.reject();
						});
					});
				}
                return deferred.promise
            }

            function userList(){
            	return $http({
					url: SETTINGS.backend + "/user/list/",
					method: "POST",
					data: {
						access_token: serviceData.access_token
					}
				});
			}

            function updateStatus(_current_status){
            	return $http({
					url: SETTINGS.backend + "/user/update_status/",
					method: "POST",
					data: {
						access_token: serviceData.access_token,
						current_status: _current_status
					}
				});
			}

			function getStatusString(_current_status){
				return {
					"1": "Working",
					"2": "On Vacation",
					"3": "Business Trip"
				}[_current_status];
			}

			function setPermission(role){
				PermRoleStore.defineRole(role, []);
			}

			function clearPermissions(){
				PermRoleStore.clearStore();
			}
        });
})();
