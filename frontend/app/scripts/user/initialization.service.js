(function() {
	'use strict';

	angular.module('user')
		.factory('InitializationService', function ($rootScope, $q, UserService) {

			var initialized = false;

			return {
				initialize: initialize
			};

			function initialize() {
				var deferred = $q.defer();
				$rootScope.waitingPromise = deferred;
				if (!initialized) {
					UserService.authenticate().finally(function () {
						initialized = true;
						deferred.resolve();
					});
				} else {
					deferred.resolve();
				}

				return deferred.promise;
			}
		});
})();
