# -*- coding: UTF-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Employer(models.Model):
    username = models.CharField(max_length=150, blank=True, null=True)
    current_status = models.CharField(max_length=1, choices=(('1', 'Working'), ('2', 'On Vacation'), ('3', 'Business Trip')))


class EmployerToken(models.Model):
    employer = models.ForeignKey(Employer, blank=True, null=True, on_delete=models.CASCADE)
    token = models.CharField(max_length=255, unique=True)
    expires = models.DateTimeField()
