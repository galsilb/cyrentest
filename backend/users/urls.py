# -*- coding: UTF-8 -*-
from django.conf.urls import url
from oauth2_provider import views as oauth2_views

import views

urlpatterns = [
    url(r'^user/login/$', views.Login.as_view()),
    url(r'^user/logout/$', views.Logout.as_view()),
    url(r'^user/authenticate/$', views.AuthenticateView.as_view()),

    url(r'^user/list/$', views.UserList.as_view()),
    url(r'^user/update_status/$', views.UpdateStatus.as_view()),
]