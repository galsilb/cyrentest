# -*- coding: UTF-8 -*-
from rest_framework import serializers
import models


class EmployerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Employer
        fields = ('pk', 'username', 'current_status')


class UpdateStatusSerializer(serializers.ModelSerializer):
    current_status = serializers.ChoiceField(choices=['1', '2', '3'])

    class Meta:
        model = models.Employer
        fields = ('current_status',)