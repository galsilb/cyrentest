# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User, Permission
from django.utils.translation import ugettext_lazy as _
from models import Employer, EmployerToken

class UserAdmin(AuthUserAdmin):
    list_display = ('username', 'email', 'name', 'is_staff', 'date_joined')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    def name(self, user):
        return u'{first} {last}'.format(first=user.first_name, last=user.last_name)

    def staff(self, user):
        return 'yes' if user.is_staff else 'no'


def user_new_unicode(self):
    return self.get_full_name()
User.__unicode__ = user_new_unicode


class EmployerAdmin(admin.ModelAdmin):
    list_display = ('username', 'current_status',)


class EmployerTokenAdmin(admin.ModelAdmin):
    list_display = ('employer', 'token', 'expires')


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Employer, EmployerAdmin)
admin.site.register(EmployerToken, EmployerTokenAdmin)
admin.site.register(Permission)
