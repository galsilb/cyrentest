# -*- coding: UTF-8 -*-
import json

from datetime import datetime, timedelta
from oauth2_provider.models import Application, AccessToken
from oauthlib.oauth2.rfc6749.tokens import random_token_generator
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.settings import oauth2_settings

from django.contrib.auth.models import User
from rest_framework.exceptions import NotFound

import serializers
import models


class Login(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        try:
            if request.data is not None and request.data['username'] is not None:
                employer = models.Employer.objects.get(username=request.data['username'])
            else:
                raise models.Employer.DoesNotExist
        except models.Employer.DoesNotExist as e:
            raise NotFound(e)

        expire_seconds = oauth2_settings.user_settings['ACCESS_TOKEN_EXPIRE_SECONDS']

        expires = datetime.now() + timedelta(seconds=expire_seconds)
        employer_token = models.EmployerToken.objects.create(
            employer=employer,
            token=random_token_generator(request),
            expires=expires)

        return Response({
            'access_token': employer_token.token,
            'token_type': 'Bearer',
            'expires_in': expire_seconds
        }, status=200)


class Logout(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        if 'access_token' in request.data:
            employer_token = models.EmployerToken.objects.get(token=request.data['access_token'])
            employer_token.delete()
            return Response(status=200)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


class AuthenticateView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        try:
            employer = get_employer_from_token(request)
            return Response(serializers.EmployerSerializer(employer).data)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class UserList(APIView):
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def get_queryset(_current_status=None):
        try:
            if _current_status is not None:
                return models.Employer.objects.filter(current_status=_current_status)
            return models.Employer.objects.all()
        except models.Employer.DoesNotExist as e:
            raise NotFound(e)

    def post(self, request, current_status=None):
        employer = get_employer_from_token(request)
        if employer is not None:
            serializer = serializers.EmployerSerializer(self.get_queryset(current_status), many=True)
            return Response(serializer.data)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


class UpdateStatus(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        employer = get_employer_from_token(request)
        if employer is not None:
            serializer = serializers.UpdateStatusSerializer(employer, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


def get_employer_from_token(request):
    try:
        if 'access_token' in request.data:
            employer_token = models.EmployerToken.objects.get(token=request.data['access_token'])
            return employer_token.employer
        return None
    except Exception as e:
        raise e
